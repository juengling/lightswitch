# Ein Python-Projekt zum "intelligenten" Steuern diverser Schaltsteckdosen im Haus


Schaltsteckdosen sind toll, Zeitschaltuhren sind auch toll. Ich wünsche mir ein Gerät, das beides in sich vereint:

* Die Außenhelligkeit oder die Uhrzeit steuert automatisches Ein-/Ausschalten des Lichts
* Die Uhrzeit steuert auf Wunsch vorrangig das Ausschalten (z.B. "um 23 Uhr alles ausschalten, egal wie dunkel es draußen ist")
* Bei Automatikbetrieb nicht alle Steckdosen gleichzeitig schalten, sondern zufällig zeitversetzt ansteuern (Sicherheitsfeature, um einen Bewohner zu simulieren)
* Manuelle Steuerung über Fernbedienung soll jederzeit möglich sein

Meiner Ansicht nach müsste man das mit einem handelsüblichen Schaltsteckdosenset und einem Raspberry Pi hinbekommen. Zusätzlich zum Pi brauchte man vermutlich "nur" :sunglasses: einen 433-MHz-Sender und einen Helligkeitssensor. Der Rest sollte mit Software lösbar sein, entsprechende Bibliotheken habe ich teilweise schon gefunden.


**Erweiterungen**

Eine Weboberfläche für die Konfiguration wäre natürlich nett, für den Anfang reicht natürlich auch eine Datei mit den Einstellungen.


**Ausfallsicherheit**

Und für den Fall, dass der Pi mal ausfällt, funktionieren die Steckdosen mit der normalen Fernbedienung immer noch, denn daran werden wir sicher keine Veränderungen vornehmen.